---
title: Design for the New
layout: "page"
order: 3
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/abaeb2c20eef-2.png)

### Faculty
Markel Cormenzana  
Mercè Rua    

### Syllabus and Learning Objectives
The learning process in the MDeF has materialised over the last few months into a personal project on the part of each student. These projects constitute socio-technical interventions on reality which take as reference the coordinates offered in the contents of the master: urban environment, digital manufacturing, technology, new design postures ... These interventions are beginning to outline possible, critical and/or desirable futures that are to be constituted as alternative normalities.
One of the pillars of the evolving body of knowledge that constitutes Transition Design is that of the theories of change, that is, the mental, theoretical and practical frameworks under which change happens and is meant. In this context, Transition Design bets on a reconquest of everydayness as a political positioning and an engine of change that builds bridges between the hitherto opposed theories that explain change from normative structures and the individual agency. 

One of the most promising cultural theories for explaining social phenomena and analysing everyday life is that of Social Practices. The analysis of present practices as well as their historical evolution and future potentialities creates fertile ground for an aesthetic policy that questions the status-quo and overcomes the crisis of imagination in which we live submerged as a civilization. We believe the adoption of this tools, mindsets and theoretical scaffolds might expand design’s ability to construct preferred futures from a systems perspective.

Throughout the course the design interventions proposed by the students will be framed under the theory of social practices as a means for:
- Understanding and reflecting on the implications of the performativity of everyday life in the creation of new normalities from a mind-body perspective and the role that design plays in that context.
- Designing paths of transition from the present (and its practices) to the desired scenarios through strategic interventions in the metaphorical, material and practical spheres through the construction of speculative prototypes (material, performative, narrative…)
- Engaging in a process to iterate, concretize and refine students’ projects integrating the learnings of Transition Design and the work on social practices.  

### Total Duration
Class hours: 9h  
Student work: 25h  
Total: 34h  

### Structure and Phases
*DAY 1 (april 26th)*     
15’ Introduction to the course.     
30’ Frameworks and theory- Transition Design and theories of change (Max-Neef needs, Social Practice Theory and Socio-technical transitions).  
2h 15’ Practice - Using the student’s projects as a reference we will use different lenses (temporal and ontological) to assess their future fitness by mapping stakeholders and their needs, understanding current ecologies of practices and deconstructing them in order to find design intervention points.   
Exercises:
- Stakeholder needs mapping
- Historical analysis of related practices
- Analysis of emerging practices
- Future practice envisioning
- Collective practice deconstruction
Homework for next session - Practice mapping and deconstruction 

*DAY 2 (may 9th)*    
40’ Check-in about the homework. Feedback circle.  
30’ Frameworks and theory- Transition design and new ways of designing. From user centered design to xenodesign. Speculative and discursive design. Performing transitions and embodied intelligence.  
1h 50’ Practice - Pulling from the practice deconstruction performed during the time between sessions we will explore the potentialities of different interventions in the present that may lead to the targeted future social practices. For this we will use a combination of tools and frames that might help iterate, test and discuss the different approaches.  
Exercises:
- Tasting of different tools for imagining alternatives: speculative prototyping, machine ontographies, mental landcapes, forced metaphors, experiential futures, performing transitions, LARP...
- Defining KPIs and measurements of impact
- Outlining of a transition path using the spatio-temporal matrix
- Defining the interventions to be designed next session

*DAY 3 (may 10th)*    
20’ Check-in about previous session. Feedback circle.  
120’ Practice - In this last part of the course we will focus in one of the mapped interventions of the previous sessions and design it from a performative perspective. Throughout this workshop each of the students will design performative situations in order to explore future social practices from other kinds of intelligences and ways of knowing such as embodied cognition and gut-level discernment.   
40’ Presentations - performance of the interventions.    

### Output
Performance + process report. 

### Grading Method
Continuous evaluation: 70%  
Final report and performance: 30%    

### Bibliography
[Transition Design Seminar. Carnegie Mellon Design School (2019).](https://transitiondesignseminarcmu.net/)    
[Xenodesignerly Ways of Knowing. Johanna Schmeer (2019).](https://jods.mitpress.mit.edu/pub/6qb7ohpt)  
[UNDERSTANDING SUSTAINABILITY INNOVATIONS: POINTS OF INTERSECTION BETWEEN THE MULTI-LEVEL PERSPECTIVE AND SOCIAL PRACTICE THEORY. Tom Hargreaves, Noel Longhurst and Gill Seyfang (2012).](https://www.academia.edu/3057996/UNDERSTANDING_SUSTAINABILITY_INNOVATIONS_POINTS_OF_INTERSECTION_BETWEEN_THE_MULTI-LEVEL_PERSPECTIVE_AND_SOCIAL_PRACTICE_THEORY)  
System Innovation and the Transition to Sustainability: Theory, Evidence and Policy. Elzen B, Geels FW, Green K, Eds (2004).  
Typology of Sociotechnical Transition Pathways. Research Policy 36. pp 54-79. Geels, Frank W. and Schott, Johan (2007).   
[Implications of Social Practice Theory for Sustainable Design. Kuijer, S.C. (2014).](https://repository.tudelft.nl/islandora/object/uuid%3Ad1662dc5-9706-4bb5-933b-75704c72ba30)  
[Designing change by living change. Kakee Scott, Jaco Quist and Conny Bakker (2012).](https://www.academia.edu/2103098/Designing_change_by_living_change)  
Extrapolation Factory Operator’s Manual. Montgomery and Woebken (2016).  
[Performing transitions within emergent paradigms. Grace Polifroni, Mercè Rua and Markel Cormenzana (2019).](https://medium.com/weareholon/performing-transitions-within-emergent-paradigms-452a63949b20)  

### MARKEL CORMENZANA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/b0408f9cef30-_FUJ2747.jpg)

### Email Address
<markel@holon.cat>  

### Twitter Account
https://twitter.com/markeloptah  

### Professional BIO
Markel Cormenzana, who defines himself as Transition Designer, is a Mechanical Engineer specialized in Product Development by the University of the Basque Country and the University of Southern Denmark (SDU). He has channeled his professional activity towards Design (Product, Service, Systems, User Experience...) and innovation to face complex and global problems such as the social, economic and environmental challenges we face as a civilization.

He is a guest lecturer in several design schools in Barcelona such as IED, BAU, Elisava or ESdesign. Research-wise he is currently involved in an exploration of embodied intelligence and performing arts language in the context of design, futures and worldmaking endeavors.

Understanding how to be a good ancestor in: [Holon](http://www.holon.cat/) and [Inedit](http://www.ineditinnova.com/)  

### MERCÈ RUA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0.jpeg)

### Email Address
<merce@holon.cat>  

### Professional BIO
Mercè is a Design researcher and strategist that works with organisations and institutions in helping them get qualitative, “human-centred” users' data as well as develop new solutions (services, experiences, products, etc.) aligned with the users’ needs, desires and behaviours.
She facilitates co-creation and innovation workshops using a unique blend of design thinking, improvisation and art of hosting. Ultimately, her drive is helping people develop the skills needed to adapt and create systemic changes together.
She is part of HOLON where she is currently working with UNEP to develop a user-centered Eco-innovation resource for SMEs in developing countries. She also collaborates with OuiShare and Ideas for Change and is involved with different change-makers' initiatives such as POC21, OSCEdays and EEP.